package com.ahmet.final_project;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class BackgroundActivationBroadcastReceiver extends BroadcastReceiver {

    //to check if the screen is off or not
    public boolean screenState;
    //counting the amount of times the power button is pressed
    private static int countLocked = 0;
    UserData db;
    private static final int PERMISSION_REQUEST_LOCATION = 0;

    LocationManager locationManager;
    double longitude = 0;
    double latitude = 0;


    Location location;

    BroadcastReceiver deliveredMessageBroadcastReceiver = new DeliveredMessageBroadcastReceiver();
    BroadcastReceiver sentMessageBroadcastReceiver = new SentMessageBroadcastReceiver();
    String finalLocation = null;

    HomeActivity homeActivity;
    String[] time = new String[2];

    Context mContext;
    public static String deviceName = "";
    public static String gOIP = "";

    @Override
    public void onReceive(final Context context, Intent intent) {
        homeActivity = new HomeActivity();
        db = new UserData(context);

        this.mContext = context;


        String notificationClicked = intent.getStringExtra("Clicked");

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //if the extra intent isn't empty then send the message
        if (notificationClicked != null) {
            //open database
            db.OnOPen();
            //get location
            gatherLocation(context);

            //get the data from db
            String contact1 = db.gatherCOL_3().toString();
            String message = db.gatherCOL_2().toString();
            String name = db.gatherCOL_1().toString();

            if (finalLocation == null) {

            } else {
                updateLocation(context);
            }

            String sent = "SENT";
            String delivered = "DELIVERED";

            //initialise pending intents for broadcast receiver to listen for whenever the message is sent and delivered
            PendingIntent sentPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(
                    sent), PendingIntent.FLAG_UPDATE_CURRENT
            );
            PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(context, 0,
                    new Intent(delivered), PendingIntent.FLAG_UPDATE_CURRENT
            );
            //register the receivers
            context.getApplicationContext().registerReceiver(sentMessageBroadcastReceiver, new IntentFilter(sent));
            context.getApplicationContext().registerReceiver(deliveredMessageBroadcastReceiver, new IntentFilter(delivered));

            //send the message
            homeActivity.sendMessage(name, message, contact1, sentPendingIntent, deliveredPendingIntent, latitude, longitude);

            db.onClose();

        } else {
        }

        //broadcast receiver on why it may take some time to receive actions, because sometimes it takes a little longer than usual
        //"Even in the case of normal broadcasts, the system may in some situations revert to delivering the broadcast one receiver at a time.
        // In particular, for receivers that may require the creation of a process, only one will be run at a time to avoid overloading the
        // system with new processes. In this situation, however, the non-ordered semantics hold: these receivers still cannot return results
        // or abort their broadcast."
        //https://developer.android.com/reference/android/content/BroadcastReceiver.html

        //send the screen state information and update the background service from service update class
        Intent i = new Intent(context, BackgroundService.class);
        //clear the tasks above of the stack and not restart a new task but simply open the app when unlocked
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_RECEIVER_FOREGROUND);
        i.putExtra("screen_state", screenState);
        //start the service essentially from background service update class
        context.startService(i);
        context.sendBroadcast(i);

        //this should start the service after restart but somehow crashes the app
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            context.startForegroundService(i);
//        }
    }

    //get the current location
    public void gatherLocation(Context context) {

        boolean is_gps_provider_enabled = false;
        boolean is_network_provider_enabled = false;

        Location gpsLocation = null;
        Location networkLocation = null;

        //location manager helps to get current location
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        is_network_provider_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            //adapted from https://developer.android.com/guide/topics/location/strategies.html
            try {
                if (is_gps_provider_enabled) {
                    //using gps provider service
                    gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (is_network_provider_enabled) {
                    //using network provider service
                    networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                //if gps and network providers are in use then get the best accuracy and use that to provide location
                if (gpsLocation != null && networkLocation != null) {
                    //checking if network has a better accuracy first, if true then use the network provided service
                    if (gpsLocation.getAccuracy() > networkLocation.getAccuracy()) {
                        //get the values to send via message
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    } else {   //otherwise use gps provider
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    }
                }
                //if one of the provider is disabled then use the other
                else {
                    if (gpsLocation != null) {
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    } else if (networkLocation != null) {
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    }
                }

            } catch (Exception ec) {
                ec.printStackTrace();
                Log.d("Error occurred", ec.getMessage());
            }

        } else

        {
            //ask for permission if access is not given
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

    public void updateLocation(Context context) {

        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //the interval which the user selected for updating location
        final int loc_update = Integer.parseInt(db.gatherCOL_7().toString()) * 1000;
        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            try

            {
                final LocationListener locationListener = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        //update the values every time the location is changed
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();

                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {
                        //if gps is enabled then use gps, default is GPS Provider
                        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        }

                    }

                    @Override
                    public void onProviderDisabled(String s) {
                        //if gps is disabled then use network provider
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        } else if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        }
                    }

                };
                //updates the location and the level of zoom level. 20 is buildings level
                locationManager.requestLocationUpdates(finalLocation, loc_update, 20, locationListener);
            } catch (
                    Exception exce)

            {
                exce.printStackTrace();
                Log.d("Error occurred", exce.getMessage());
            }
        } else

        {
            //ask for permission if access is not given
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

}
