package com.ahmet.final_project;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;

import android.support.v4.app.NotificationCompat;


public class BackgroundService extends Service /*implements WifiP2pManager.PeerListListener, WifiP2pManager.ConnectionInfoListener*/ {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    UserData db;

    HomeActivity homeActivity;

    P2POnConnection p2POnConnection;

    Notification notification;

    @Override
    public void onCreate() {
        super.onCreate();

        p2POnConnection = new P2POnConnection();

        //registering the states/actions
        //Specifying the type of intent, screen on and off
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        //trigger when the screen is off
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        //trigger when the device restarts
        intentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
        //checks if the user wakes up the device
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);

        //Broadcast receiver to register the screen state
        BroadcastReceiver broadcastReceiver = new BackgroundActivationBroadcastReceiver();
        registerReceiver(broadcastReceiver, intentFilter);

        homeActivity = new HomeActivity();

    }

    //non-removable notification, sends an extra intent for broadcast receiver to listen for any action when clicked
    public void lockScreenNotification() {

        String notfirsttime = "1";

        db = new UserData(getApplicationContext());
        db.OnOPen();
        String notFirstTimeTrue = db.getNotFirstTime(notfirsttime);

        //if the user completed the setup then build the notification
        if (notfirsttime.equals(notFirstTimeTrue)) {

            Intent intent = new Intent(this, BackgroundActivationBroadcastReceiver.class);
            //passing this value to powerbuttonactivation class so it gets called when clicked
            intent.putExtra("Clicked", "NotificationClicked");
            //pendingintent will give permission to notification manager to use the specified service and apply actions via the broadcast receiver
            PendingIntent pendingIntent =
                    PendingIntent.getBroadcast(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = "Channel1";
            String channelName = "Final Year Project buildNotification Channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            //adapted from -- https://stackoverflow.com/questions/43093260/notification-not-showing-in-oreo
            //create notification channel in order to build notification for devices have api 26 and above. Android Oreo +
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                NotificationChannel notificationChannel = new NotificationChannel(
                        channelId, channelName, importance);
                //disable vibration, it seems like a bug in android oreo where it doesn't work even if the vibration is turned off.
                notificationChannel.setVibrationPattern(new long[]{0});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);

                notification = new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_tick)
                        .setColor(Color.RED)
                        .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                        .setContentTitle("Send Emergency message now")
                        //user cannot remove the notification
                        .setOngoing(true).setContentIntent(pendingIntent).build();

                startForeground(1, notification);

            } else {
                //adapted from https://developer.android.com/training/notify-user/build-notification.html#notify
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(this, channelId)
                                .setSmallIcon(R.drawable.ic_tick)
                                .setColor(Color.RED)
                                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                                .setContentTitle("Send Emergency message now")
                                //user cannot remove the notification
                                .setOngoing(true)
                                .setPriority(Notification.PRIORITY_MAX);
                //send pending intent to BackgroundActivationBroadcastReceiver broadcastreceiver so it listens for any clicks on the notification
                builder.setContentIntent(pendingIntent);
                //Build the notification
                notificationManager.notify(0, builder.build());
            }
        } else {
        }

    }

    public void Notification() {

        //setting the ui of the notification
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_tick)
                        .setColor(Color.RED)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setContentTitle("SOS")
                        .setContentText("HELP!");

        //Show the notification
        NotificationManager notificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(2, builder.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        db = new UserData(getApplicationContext());
        db.OnOPen();
        //build the notification when the service starts
        lockScreenNotification();
        db.onClose();
        //restart the service if its killed
        return START_STICKY;
    }
}


