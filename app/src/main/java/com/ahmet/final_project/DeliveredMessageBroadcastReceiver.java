package com.ahmet.final_project;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

import java.util.Timer;
import java.util.TimerTask;

//Broadcast receiver listening for the sms delivering action and output message depending on the result
public class DeliveredMessageBroadcastReceiver extends BroadcastReceiver {
    String notificationTitle = "Successfully Message Delivered";
    String notificationBody = "Message delivered to: ";
    String updateMessageTitle = "Update Message Delivered";

    int countTimer = 0;
    double latitude = 0;
    double longitude = 0;
    String contact1 = "";
    HomeActivity homeActivity;
    UserData db;

    String finalLocation = null;

    String message;
    String name;
    Timer timer;

    @Override
    public void onReceive(final Context context, Intent intent) {
        homeActivity = new HomeActivity();
        db = new UserData(context);
        contact1 = db.gatherCOL_3().toString();

        message = db.gatherCOL_2().toString();
        name = db.gatherCOL_1().toString();

        String sent = "SENT";
        String delivered = "DELIVERED";
        //initialise pending intents for broadcast receiver to listen for whenever the message is sent and delivered
        final PendingIntent sentPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(
                sent), 0);
        final PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(delivered), 0);

        BroadcastReceiver deliveredMessageBroadcastReceiver = new DeliveredMessageBroadcastReceiver();

        context.registerReceiver(deliveredMessageBroadcastReceiver, new IntentFilter(delivered));

        //get location
        gatherLocation(context);

        //adapted from https://stackoverflow.com/questions/29118625/android-sms-manager-delivery-report-and-sent-report-not-received
        switch (getResultCode()) {
            //if message is delivered then notify the user
            case Activity.RESULT_OK:
                db.OnOPen();
                //Build notification after a successful sent message
                homeActivity.buildNotification(context, 1, R.drawable.ic_tick, Color.RED, notificationTitle, notificationBody + contact1);

                //get location if its null else start getting location update
                if (finalLocation == null) {
                    gatherLocation(context);

                } else {
                    updateLocation(context);
                }

                //delay for 5 seconds to get the accurate location, without the delay it provides an inaccurate location
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        //send message
                        sendUpdateMessage(context);
                    }
                }, 5000);

                db.onClose();
                break;
            case Activity.RESULT_CANCELED:

                //if the message isn't delivered then re-send it after 5 seconds

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        //will stop after it sent
                        if (countTimer == 1) {
                            timer.cancel();
                            timer.purge();
                            db.onClose();
                        } else {
                            //send the message
                            homeActivity.sendMessage(name, message, contact1, sentPendingIntent, deliveredPendingIntent, latitude, longitude);
                            countTimer++;
                            db.onClose();
                        }
                    }
                    //sends after 5 seconds.
                }, 5000);

                break;
        }

    }

    public void sendUpdateMessage(final Context context) {
        //the interval for sending update message
        final int loc_update = Integer.parseInt(db.gatherCOL_7().toString()) * 1000;

        try {
            //adapted from https://developer.android.com/reference/java/util/TimerTask.html
            //timer to send location updates for no more than 2 times
            final Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //it will send 2 updates
                    if (countTimer == 2) {
                        timer.cancel();
                        timer.purge();

                    } else {
                        //send the sms
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(contact1, null, "Location Update: "
                                + "http://maps.google.com/?q=" + latitude + "," + longitude + "", null, null);
                        //build notification after every update
                        homeActivity.buildNotification(context, 2, R.drawable.ic_tick, Color.RED, updateMessageTitle, notificationBody + contact1);
                        countTimer++;
                    }
                }
                //sends every interval of whatever the user chose at the setup
            }, loc_update, loc_update);
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    //get the current location
    public void gatherLocation(Context context) {

        boolean is_gps_provider_enabled = false;
        boolean is_network_provider_enabled = false;

        Location gpsLocation = null;
        Location networkLocation = null;

        //location manager to call location services
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        is_network_provider_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            //adapted from https://developer.android.com/guide/topics/location/strategies.html

            try {
                if (is_gps_provider_enabled) {
                    //using gps provider service
                    gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (is_network_provider_enabled) {
                    //using network provider service
                    networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                //if gps and network providers are in use then get the best accuracy and use that to provide location
                if (gpsLocation != null && networkLocation != null) {
                    //checking if network has a better accuracy first, if true then use the network provided service
                    //the more the getAccuracy() value is, the worse the actual accuracy is
                    if (gpsLocation.getAccuracy() > networkLocation.getAccuracy()) {
                        //get the values to send via message
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    } else {
                        //otherwise use gps provider
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    }
                }
                //if one of the provider is disabled then use the other
                else {
                    if (gpsLocation != null) {
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    } else if (networkLocation != null) {
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    }
                }

            } catch (Exception ec) {
                ec.printStackTrace();
            }

        } else

        {
            //ask for permission if access is not given
            // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

    //Update the location
    public void updateLocation(Context context) {

        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //the interval for sending update message
        final int loc_update = Integer.parseInt(db.gatherCOL_7().toString()) * 1000;
        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            try

            {
                final LocationListener locationListener = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        //update the values when location is changed
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();

                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {
                        //use gps if its enabled
                        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        }

                    }

                    @Override
                    public void onProviderDisabled(String s) {
                        //if gps is disabled then use network provider
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        } else if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        }
                    }

                };
                //updates the location and the level of zoom level. 20 is buildings level
                locationManager.requestLocationUpdates(finalLocation, loc_update, 20, locationListener);
            } catch (
                    Exception exce)

            {
                exce.printStackTrace();
            }
        } else

        {
            //ask for permission if access is not given
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

}
