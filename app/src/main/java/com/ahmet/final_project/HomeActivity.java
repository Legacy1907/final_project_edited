package com.ahmet.final_project;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback {

    //    Button showButton;
    Button sosButton;
    Button locationUpdateButton;
    Button contactsettingButton;
    Button activationButton;
    ImageButton activationSettinginformationButton;

    TextView contact1TextV;
    TextView contact2TextV;
    TextView contact3TextV;

    private static final int PERMISSION_REQUEST_LOCATION = 0;
    private static final int CONTACT_REQUEST_CODE = 1;

    TextView contact1TextView;
    TextView contact2TextView;
    TextView contact3TextView;

    String sFinalContact1 = "";
    String sFinalContact2 = "";
    String sFinalContact3 = "";

    double longitude = 0;
    double latitude = 0;
    UserData db;
    String finalLocation = null;
    private GoogleMap mMap;
    LatLng currentLocation = null;

    String networkSSID = null;

    String whatsappMessage = "";
    BroadcastReceiver broadcastReceiver;

    BroadcastReceiver sentMessageBroadcastReceiver = new SentMessageBroadcastReceiver();
    BroadcastReceiver deliveredMessageBroadcastReceiver = new DeliveredMessageBroadcastReceiver();

    public boolean isWifiP2pEnabled = false;

    public static final String TAG = "TAG";

    String contact1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment
                .getMapAsync(this);

        //initialize the buttons and textviews
        sosButton = (Button) findViewById(R.id.sosButton);
//        showButton = (Button) findViewById(R.id.showButton);

//        locationUpdateButton = (Button) findViewById(R.id.locationSettingsButton);
//        contactsettingButton = (Button) findViewById(R.id.contactsButton);
//        activationButton = (Button) findViewById(R.id.activationButton);

        contact1TextV = (TextView) findViewById(R.id.contactSetting1Textview);
        contact2TextV = (TextView) findViewById(R.id.contactSetting2Textview);
        contact3TextV = (TextView) findViewById(R.id.contactSetting3Textview);

        //get Contact 1 from database
        db = new UserData(this);
        contact1 = db.gatherCOL_3().toString();

        //opens location update setting dialog
//        locationUpdateSettings();
//        //opens contact setting dialog
//        contactSettings();
//        //opens activation setting dialog
//        activationSettings();

        //Service//
        //Specifying the type of intent, screen on and off
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        //adding screen off to trigger when the screen is off
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        //for startup service
        intentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
        //checks if the user wakes up the device
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        //Broadcast receiver to register the screen state
        broadcastReceiver = new BackgroundActivationBroadcastReceiver();
        registerReceiver(broadcastReceiver, intentFilter);

        whatsappMessage = "I'm " + "ahmet" + " and " + "I'm in danger" + " My location is: "
                //if comma doesnt work then use encoding for the link
                + "http://maps.google.com/?q=" + latitude + "," + longitude + "";
        //show the database values
//        Show();

        //Send the sos message when the button is clicked
        sosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //vibrate on click
                Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(1000);
                //get the data from db
                String contact = db.gatherCOL_3().toString();
                String message = db.gatherCOL_2().toString();
                String name = db.gatherCOL_1().toString();

                //the strings for sending intent to Sent and DeliveredBroadcastReceiver
                String SENT = "SENT";
                String DELIVERED = "DELIVERED";

                //initialise pending intents for broadcast receiver to listen for whenever the message is sent and delivered
                PendingIntent sentPendingIntent = PendingIntent.getBroadcast(HomeActivity.this, 0, new Intent(
                        SENT), PendingIntent.FLAG_UPDATE_CURRENT);

                PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(HomeActivity.this, 0,
                        new Intent(DELIVERED), PendingIntent.FLAG_UPDATE_CURRENT);

                //register receivers so it can start receiving and create the intents
                registerReceiver(sentMessageBroadcastReceiver, new IntentFilter(SENT));
                registerReceiver(deliveredMessageBroadcastReceiver, new IntentFilter(DELIVERED));

                boolean is_gps_provider_enabled = false;
                //location manager helps to get current location
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                //setting boolean values for either gps or network availability
                is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                //if gps isnt enabled, open location settings to enable it
                if (!is_gps_provider_enabled) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                    builder.setCancelable(true);
                    builder.setTitle("Location Services");
                    builder.setMessage("Please turn on location services.");
                    builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int id) {
                            //open location services setting
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            dialogInterface.cancel();
                        }
                    });
                    builder.show();

                } else {
                    //send the message
                    SendAlertMessage sendAlertMessage = new SendAlertMessage(name, message, contact, sentPendingIntent, deliveredPendingIntent, latitude, longitude);
//                    sendMessage(name, message, contact, sentPendingIntent, deliveredPendingIntent, latitude, longitude);
                    db.onClose();
                }
            }
        });

        //get current location
        gatherLocation();

        boolean is_gps_provider_enabled = false;
        //location manager helps to get current location
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        //if gps isnt enabled then open location services settings
        if (!is_gps_provider_enabled) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Location Services");
            builder.setMessage("Please turn on location services.");
            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int id) {
                    //open location services setting
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialogInterface.cancel();
                }
            });
            builder.show();

        }

        if (finalLocation == null) {

            gatherLocation();
        } else {
            updateLocation();
        }
        db.onClose();
    }

    //inflating the menu on action bar within activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    //action bar button options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //when clicked, it opens settings dialog box
            case R.id.settingsActionBar:

                //new alert dialog
                AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.settings_dialog_box, null);
                //initialise buttons and textviews
                Button contactSettingButton = (Button) view1.findViewById(R.id.contactSettingButton);
                Button locationSettingButton = (Button) view1.findViewById(R.id.locationSettingButton);
                Button activationSettingButton = (Button) view1.findViewById(R.id.activationSettingButton);

                contactSettingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        contactSettings();
                    }
                });
                locationSettingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        locationUpdateSettings();
                    }
                });
                activationSettingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activationSettings();
                    }
                });

//                set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(true);

                dialog.setNegativeButton("Go Back", null);

                //create and show the dialog
                final AlertDialog alert = dialog.create();
                alert.show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    public void onResume() {
        super.onResume();
        //check if location services is turned off
        boolean is_gps_provider_enabled = false;
        //location manager helps to get current location
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!is_gps_provider_enabled) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Location Services");
            builder.setMessage("Please turn on location services.");
            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int id) {
                    //open location services setting
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    dialogInterface.cancel();
                }
            });
            builder.show();

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //showing google map
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        //get refresh location
        final int loc_refresh = (Integer.parseInt(db.gatherCOL_8().toString()) * 1000) * 60;

        //permission to access location
        int permissionLocation = ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        //location manager to call location services
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //default provider is network for location updates
        finalLocation = LocationManager.GPS_PROVIDER;

        //if permission granted
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            try {
                //adapted from https://developers.google.com/maps/documentation/android-api/map

                //use lat long to define the location
                currentLocation = new LatLng(latitude, longitude);

                //moves the screen towards the marker with a zoom level of 15 showing streets
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));

                //my location button on top right corner
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        return false;
                    }
                });
                mMap.setOnMyLocationClickListener(new GoogleMap.OnMyLocationClickListener() {
                    @Override
                    public void onMyLocationClick(@NonNull Location location) {

                    }
                });
            } catch (Exception ec) {
                ec.printStackTrace();
                showMessage("Error occurred", ec.getMessage());
            }

        } else {
            //ask for permission if access is not given
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }


        db.onClose();

    }

    //testing purposes, showing user data
//    public void Show() {
//        db = new UserData(getApplicationContext());
//
//        showButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    // Show all data
//                    String data = db.gatherAllData();
//                    showMessage("User Information", data);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    showMessage("Error", e.gatherCOL_2());
//                }
//
//            }
//        });
//    }

    public void sendMessage(String name, String message, String contact, PendingIntent sentIntent, PendingIntent deliveryIntent, double latitude, double longitude) {

        if (contact == null || message == null || name == null) {

        } else {
            try {

                //adapted from https://www.tutorialspoint.com/android/android_sending_sms.htm
                //send the sms including, name + message + current location of a google maps link + sent and delivered intents to check if its sent and delivered
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(contact, null, "I'm " + name + " and " + message + " My location is: "
                        + "http://maps.google.com/?q=" + latitude + "," + longitude + "", sentIntent, deliveryIntent);

            } catch (Exception ex) {
                ex.printStackTrace();
            }


        }
    }

    //build notification with the method
    public void buildNotification(Context context, int channelID, int icon, int color, String title, String body) {
        //adapted from https://developer.android.com/training/notify-user/build-notification.html
        //setting the ui of the notification
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(icon)
                        .setColor(color)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                        .setContentTitle(title)
                        .setContentText(body);
        //Build the notification
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(channelID, builder.build());

    }

    //get the current location
    public void gatherLocation() {

        boolean is_gps_provider_enabled = false;
        boolean is_network_provider_enabled = false;

        Location gpsLocation = null;
        Location networkLocation = null;

        //location manager helps to get current location
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        is_network_provider_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            //adapted from https://developer.android.com/guide/topics/location/strategies.html
            try {
                if (is_gps_provider_enabled) {
                    //using gps provider service
                    gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (is_network_provider_enabled) {
                    //using network provider service
                    networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                //if gps and network providers are in use then get the best accuracy and use that to provide location
                if (gpsLocation != null && networkLocation != null) {
                    //checking if network has a better accuracy first, if true then use the network provided service
                    if (gpsLocation.getAccuracy() > networkLocation.getAccuracy()) {
                        //get the values to send via message
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    } else {   //otherwise use gps provider
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    }
                }
                //if one of the provider is disabled then use the other
                else {
                    if (gpsLocation != null) {
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    } else if (networkLocation != null) {
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    }
                }

            } catch (Exception ec) {
                ec.printStackTrace();
                showMessage("Error occurred", ec.getMessage());
            }

        } else

        {
            //ask for permission if access is not given
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

    public void updateLocation() {

        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //the interval which the user selected for updating location
        final int loc_update = Integer.parseInt(db.gatherCOL_7().toString()) * 1000;
        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            try

            {
                final LocationListener locationListener = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        //update the values every time the location is changed
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();

                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {
                        //if gps is enabled then use gps, default is GPS Provider
                        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        }

                    }

                    @Override
                    public void onProviderDisabled(String s) {
                        //if gps is disabled then use network provider
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        } else if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        }
                    }

                };
                //updates the location and the level of zoom level. 20 is buildings level
                locationManager.requestLocationUpdates(finalLocation, loc_update, 20, locationListener);
            } catch (
                    Exception exce)

            {
                exce.printStackTrace();
                showMessage("Error occurred", exce.getMessage());
            }
        } else

        {
            //ask for permission if access is not given
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

    //adapted from https://developer.android.com/training/permissions/requesting.html
    //getting the grant from the user and handling the response
    @Override
    public void onRequestPermissionsResult(int request, String[] permission, int[] grant) {
        super.onRequestPermissionsResult(request, permission, grant);
        //checks for location permission
        switch (request) {

            case PERMISSION_REQUEST_LOCATION: {
                if (grant.length >= 0 && grant[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    showMessage("Grant Permission", "Application will not fully function if you don't grant permissions.");
                }

            }

        }
    }

    //building alert dialog to show dialog messages
    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.setNeutralButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    //update location preferences
    public void locationUpdateSettings() {
        db.OnOPen();
        //get name from db so it can update
        final String name = db.gatherCOL_1().toString();


        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        //inflate the dialog layout to import it into alert dialog in order to view
        LayoutInflater layoutInflater = getLayoutInflater();
        //when inflated, then able to view the settings screen
        View view1 = layoutInflater.inflate(R.layout.location_update_setting_screen, null);
        //initialise the spinners
        final Spinner spinnerUpdate = (Spinner) view1.findViewById(R.id.locationUpdateSettingUpdateSpinner);
        final Spinner spinnerHistory = (Spinner) view1.findViewById(R.id.locationUpdateSettingHistorySpinner);
        final Spinner spinnerRefresh = (Spinner) view1.findViewById(R.id.locationUpdateSettingRefreshSpinner);

        ImageButton locationsettinginformationbutton = (ImageButton) view1.findViewById(R.id.locationupdateSettingInformationButton);

        //information dialog box within
        locationsettinginformationbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.information_dialog_box, null);

                EditText informationText = (EditText) view1.findViewById(R.id.informationEditText);
                //providing information
                String information = getResources().getString(R.string.location_update_Information);
                informationText.setText(information);
                //set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(false);
                //create clickable button
                dialog.setPositiveButton("Okay", null);

                final AlertDialog alert = dialog.create();
                alert.show();
            }
        });

        try {
            //adapted from https://developer.android.com/guide/topics/ui/controls/spinner.html
            //Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                    R.array.location_update, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerUpdate.setAdapter(adapter);

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getApplicationContext(),
                    R.array.refresh_location, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerRefresh.setAdapter(adapter2);

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(getApplicationContext(),
                    R.array.location_history, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerHistory.setAdapter(adapter3);

        } catch (Exception e) {
            e.printStackTrace();
            showMessage("Error", e.getMessage());
        }
        //set custom layout in dialog
        dialog.setView(view1);

        dialog.setCancelable(true);
        //create clickable buttons
        dialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //get from spinners
                String setLoca_update = spinnerUpdate.getSelectedItem().toString();
                String setLoca_refresh = spinnerRefresh.getSelectedItem().toString();
                String setLoca_history = spinnerHistory.getSelectedItem().toString();
                try {
                    //update location update
                    boolean updatedData = db.updateLocationUpdates(name, setLoca_update, setLoca_refresh, setLoca_history);

                    if (updatedData == true) {

                        showMessage("Information", "Data Updated!");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    showMessage("Error", ex.getMessage());

                }
            }
        });
        dialog.setNegativeButton("Cancel", null);

        final AlertDialog alert = dialog.create();
        alert.show();
        db.onClose();

    }

    public void activationSettings() {
        db.OnOPen();
        final String name = db.gatherCOL_1().toString();

        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        //inflate the dialog layout to import it into alert dialog in order to view
        LayoutInflater layoutInflater = getLayoutInflater();
        //when inflated, then able to view the settings screen
        View view1 = layoutInflater.inflate(R.layout.activation_setting_screen, null);
        //initialise spinner
        final Spinner spinnerPowerbutton = (Spinner) view1.findViewById(R.id.activation_setting_power_spinner);
        //initialise information button
        activationSettinginformationButton = (ImageButton) view1.findViewById(R.id.activationSettingInformationButton);

        //information dialog box
        activationSettinginformationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.information_dialog_box, null);

                EditText informationText = (EditText) view1.findViewById(R.id.informationEditText);
                //providing information
                String information = getResources().getString(R.string.setup3_description);
                informationText.setText(information);
                //set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(false);
                //create clickable button
                dialog.setPositiveButton("Okay", null);

                final AlertDialog alert = dialog.create();
                alert.show();
            }
        });

        try {
            //https://developer.android.com/guide/topics/ui/controls/spinner.html
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                    R.array.power_button_press, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerPowerbutton.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
            showMessage("Error", e.getMessage());
        }
        //set custom layout in dialog
        dialog.setView(view1);

        dialog.setCancelable(true);
        //create clickable buttons
        dialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String setPowerbuttonSpinnervalue = spinnerPowerbutton.getSelectedItem().toString();

                //update db
                boolean updated = db.updateActivation(name, setPowerbuttonSpinnervalue);
                if (updated == true) {
                    showMessage("Information", "Successfully updated!");
                }

            }
        });
        dialog.setNegativeButton("Cancel", null);

        final AlertDialog alert = dialog.create();
        alert.show();
        db.onClose();

    }

    public void contactSettings() {
        db.OnOPen();
        //get name from db so it can update using this
        final String name = db.gatherCOL_1().toString();

        final String dbContact1 = db.gatherCOL_3().toString();
        final String dbContact2 = db.gatherCOL_4().toString();
        final String dbContact3 = db.gatherCOL_5().toString();

        //new alert dialog
        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        //inflate the dialog layout to import it into alert dialog in order to view
        LayoutInflater layoutInflater = getLayoutInflater();
        //when inflated, then able to view the settings screen
        View view1 = layoutInflater.inflate(R.layout.contact_setting_screen, null);
        //initialise buttons and textviews
        Button contact1Button = (Button) view1.findViewById(R.id.contactSettingaddContact1Button);
        Button manuallyAddContact = (Button) view1.findViewById(R.id.contactSettingaddContactManuallyButton);

        contact1TextView = (TextView) view1.findViewById(R.id.contactSetting1Textview);
        contact2TextView = (TextView) view1.findViewById(R.id.contactSetting2Textview);
        contact3TextView = (TextView) view1.findViewById(R.id.contactSetting3Textview);

//                contact1TextView.setText(dbContact1);
//                contact2TextView.setText(dbContact2);
//                contact3TextView.setText(dbContact3);

        try {
            //on contact1 button clicked, it opens the contacts list
            contact1Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //opens contact list and allows the user to select limited multiple contacts
                    Intent contactList = new Intent("intent.action.INTERACTION_TOPMENU");
                    contactList.putExtra("additional", "phone-multi");
                    contactList.putExtra("maxRecipientCount", 3);
                    contactList.putExtra("FromMMS", true);

                    startActivityForResult(contactList, CONTACT_REQUEST_CODE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            showMessage("Error", e.getMessage());
        }

        manuallyAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //new alert dialog
                AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.add_contact_manually, null);
                //initialise buttons and textviews
                final EditText contact1EditText = (EditText) view1.findViewById(R.id.contact1EditText);
                final EditText contact2EditText = (EditText) view1.findViewById(R.id.contact2EditText);
                final EditText contact3EditText = (EditText) view1.findViewById(R.id.contact3EditText);

                //set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(true);
                dialog.setPositiveButton("Add Contacts", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String checkContact1 = contact1EditText.getText().toString();
                        String checkContact2 = contact2EditText.getText().toString();
                        String checkContact3 = contact3EditText.getText().toString();

                        //if the edittext is empty then warn the user and dont add
                        if (checkContact1.equals("")){
                            Toast.makeText(HomeActivity.this, "At least 1 contact needs to be entered to continue.", Toast.LENGTH_LONG).show();
                        }
                        //if contact 1 doesnt start with 0 then warn the user and dont add
                        else if (!checkContact1.startsWith("0")){
                            Toast.makeText(HomeActivity.this, "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        //if contact 2 isnt empty and doesnt start with 0 then dont add
                        else if (!checkContact2.equals("") && !checkContact2.startsWith("0")){
                            Toast.makeText(HomeActivity.this, "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        //if contact 3 isnt empty and doesnt start with 0 then dont add
                        else if (!checkContact3.equals("") && !checkContact3.startsWith("0")){
                            Toast.makeText(HomeActivity.this, "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        else
                        {
                            //add it to contact text view
                            contact1TextView.setText(checkContact1);
                            contact2TextView.setText(checkContact2);
                            contact3TextView.setText(checkContact3);

                            Toast.makeText(HomeActivity.this, "Contacts added!", Toast.LENGTH_LONG).show();
                        }

                    }
                });
                dialog.setNegativeButton("Cancel", null);

                //create and show the dialog
                final AlertDialog alert = dialog.create();
                alert.show();


            }
        });
        //set custom layout in dialog
        dialog.setView(view1);

        dialog.setCancelable(true);
        //create clickable buttons
        dialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (contact1TextView.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Nothing's changed! At least 1 contact needs to be selected.", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        //update contacts
                        boolean updatedData = db.updateContacts(name, sFinalContact1, sFinalContact2, sFinalContact3);

                        if (updatedData == true) {
                            //output message for successful update
                            showMessage("Information", "Data Updated!");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        showMessage("Error", ex.getMessage());

                    }
                }
            }
        });
        dialog.setNegativeButton("Cancel", null);

        //create and show the dialog
        final AlertDialog alert = dialog.create();
        alert.show();
        db.onClose();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //comparing the request code to allow picking contacts
        if (requestCode == CONTACT_REQUEST_CODE && resultCode == RESULT_OK) {

            //adapted from https://stackoverflow.com/questions/33289587/contact-intent-with-multiple-select
            //it gets the data of the selected contact
            Bundle bundle = data.getExtras();

            String result = bundle.getString("result");
            ArrayList<String> contacts = bundle.getStringArrayList("result");

            //if max amount of contacts aren't selected then only add the ones that are selected
            //so if contact equals to 1 then select that contact
            if (contacts.size() == 1) {

                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();

                //add it to contact textview
                contact1TextView.setText(sContact);
                //add it to final contact to save to the database
                sFinalContact1 = sContact;
            } else {

            }
            if (contacts.size() == 2) {
                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();
                //add it to contact text view
                contact1TextView.setText(sContact);

                sFinalContact1 = sContact;

                //only add the number and trim the rest which shows the id.
                String contact2 = contacts.get(1).toString();
                String sContact2 = contact2.substring(contact2.indexOf(";") + 1);
                sContact2.trim();
                //add it to contact text view
                contact2TextView.setText(sContact2);

                sFinalContact2 = sContact2;


            } else {

            }
            if (contacts.size() == 3) {
                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();
                //add it to contact text view
                contact1TextView.setText(sContact);

                sFinalContact1 = sContact;


                //only add the number and trim the rest which shows the id.
                String contact2 = contacts.get(1).toString();
                String sContact2 = contact2.substring(contact2.indexOf(";") + 1);
                sContact2.trim();
                //add it to contact text view
                contact2TextView.setText(sContact2);

                sFinalContact2 = sContact2;


                //only add the number and trim the rest which shows the id.
                String contact3 = contacts.get(2).toString();
                String sContact3 = contact3.substring(contact3.indexOf(";") + 1);
                sContact3.trim();
                //add it to contact text view
                contact3TextView.setText(sContact3);

                sFinalContact3 = sContact3;


            } else {

            }

        }

    }

}