package com.ahmet.final_project;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;

//listens for wifi direct connections
public class P2PBroadcastReceiver extends BroadcastReceiver {


    private WifiP2pManager manager;
    private Channel channel;
    private SentMessageBroadcastReceiver sentMessageBroadcastReceiver;
    HomeActivity homeActivity;
    P2PPeerListener p2PPeerListener;
    P2POnConnection p2POnConnection;

    private WifiP2pManager.PeerListListener peerListListener;

    //getting the connection from the sentMessageBroadcastReceiver to listen
    public P2PBroadcastReceiver(WifiP2pManager manager, Channel channel,
                                SentMessageBroadcastReceiver sentMessageBroadcastReceiver) {
        super();
        this.manager = manager;
        this.channel = channel;
        this.sentMessageBroadcastReceiver = sentMessageBroadcastReceiver;

    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        homeActivity = new HomeActivity();
        // p2p peerlistener and onconnection classes to listen for any changes in peers and connections
        p2PPeerListener = new P2PPeerListener();
        p2POnConnection = new P2POnConnection();

        //adapted from https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html
        //and https://developer.android.com/guide/topics/connectivity/wifip2p.html
        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            //checking if wifi p2p is supported and turned on
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                //set to true
//                backgroundService.WifiP2PEnabled(true);
            } else {
//                backgroundService.WifiP2PEnabled(false);
            }

        }
        //checks for changed peers in the list
        else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            //finding and alter the peers
            if (manager != null) {
                manager.requestPeers(channel, p2PPeerListener);
            }

        }
        //checks for connections
        else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

            if (manager == null) {
                return;
            }
            //get network info to check for connection changes
            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {

                manager.requestConnectionInfo(channel, p2POnConnection);

            } else {
                //disconnection

            }

        }//checks for wifi state change
        else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {

        }

    }
}
