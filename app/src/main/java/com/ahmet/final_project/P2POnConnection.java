package com.ahmet.final_project;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.util.Date;

public class P2POnConnection implements WifiP2pManager.ConnectionInfoListener {

    public WifiP2pDevice wifiP2pDevice;
    WifiP2pInfo wifiP2pInfo;

    P2PPeerListener p2PPeerListener;

    HomeActivity homeActivity;
    SentMessageBroadcastReceiver sentMessageBroadcastReceiver;
    BackgroundService backgroundService;

    int notificationChannel = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

    public Boolean isConnected = false;

    public P2POnConnection() {

        sentMessageBroadcastReceiver = new SentMessageBroadcastReceiver();
        p2PPeerListener = new P2PPeerListener();

        homeActivity = new HomeActivity();

        //if peers are available setup the connection
        if (p2PPeerListener.isPeersAvailable == true) {
//            for (int i = 0; i < p2PPeerListener.peersList.size(); i++) {
//                wifiP2pDevice = (WifiP2pDevice) p2PPeerListener.peersList.get(i);
////            String deviceName=wifiP2pDevice.deviceName;
////            int devicestatus=wifiP2pDevice.status;
//
//
//                //setup the connection and obtain peer
//                WifiP2pConfig wifiP2pConfig = new WifiP2pConfig();
//                wifiP2pConfig.deviceAddress = wifiP2pDevice.deviceAddress;
//                wifiP2pConfig.wps.setup = WpsInfo.PBC;
//                //peer to be client
////            wifiP2pConfig.groupOwnerIntent = 15;
//
//                sentMessageBroadcastReceiver.P2PConnect();
        }
    }
//    }

    //adapted from https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
        this.wifiP2pInfo = wifiP2pInfo;

        wifiP2pDevice = p2PPeerListener.wifiP2pDevice;

        backgroundService = new BackgroundService();

        homeActivity = new HomeActivity();

        Log.d("TAG", "SOS message sent");

        //when the connection is successful a group will be formed with no password request
        //the group owner will be the one who started discovery and be able to send message
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.isGroupOwner) {

            if (wifiP2pDevice != null) {
                Log.d("TAG", "SOS message sent" + wifiP2pDevice.deviceName);
            }
            else{
                Log.d("TAG", "SOS message sent");

            }

            //the other device will be the client in this case and receive the message
        } else if (wifiP2pInfo.groupFormed) {
            Log.d("TAG", "SOS message sent");

        }

    }
}
