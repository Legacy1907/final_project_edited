package com.ahmet.final_project;


import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class P2PPeerListener implements WifiP2pManager.PeerListListener {

    public List peersList = new ArrayList();
    public WifiP2pDevice wifiP2pDevice = null;
    HomeActivity homeActivity;

    public boolean isPeersAvailable = false;
    SentMessageBroadcastReceiver sentMessageBroadcastReceiver;

    public P2PPeerListener() {

    }

    //https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html
    @Override
    public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList) {
        homeActivity = new HomeActivity();
        sentMessageBroadcastReceiver = new SentMessageBroadcastReceiver();

        //if discovery started then obtain peers
        if (sentMessageBroadcastReceiver.startedDiscovery == true) {

            peersList.clear();
            //add it to the list
            peersList.addAll(wifiP2pDeviceList.getDeviceList());

            for (int i = 0; i < peersList.size(); i++) {
                wifiP2pDevice = (WifiP2pDevice) peersList.get(i);

                //setup the connection and obtain peer
                WifiP2pConfig wifiP2pConfig = new WifiP2pConfig();
                wifiP2pConfig.deviceAddress = wifiP2pDevice.deviceAddress;
                wifiP2pConfig.wps.setup = WpsInfo.PBC;
                //peer to be client
                wifiP2pConfig.groupOwnerIntent = 0;

                //connect to the peer
                sentMessageBroadcastReceiver.P2PConnect();
                isPeersAvailable = true;
            }

            if (peersList.size() == 0) {
                Log.d("Debug", "No Peers Available");
                return;
            }

        }
    }

    public interface IP2PActionListener {

        void P2PConnect();
    }
}
