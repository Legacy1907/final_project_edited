package com.ahmet.final_project;

import android.app.PendingIntent;
import android.telephony.SmsManager;

public class SendAlertMessage {

    public SendAlertMessage (String name, String message, String contact, PendingIntent sentIntent, PendingIntent deliveryIntent, double latitude, double longitude){

        try {

            //adapted from https://www.tutorialspoint.com/android/android_sending_sms.htm
            //send the sms including, name + message + current location of a google maps link + sent and delivered intents to check if its sent and delivered
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(contact, null, "I'm " + name + " and " + message + " My location is: "
                    + "http://maps.google.com/?q=" + latitude + "," + longitude + "", sentIntent, deliveryIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
