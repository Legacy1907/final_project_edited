package com.ahmet.final_project;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

//Broadcast receiver listening for the sms sending action and output message depending on the result
public class SentMessageBroadcastReceiver extends BroadcastReceiver implements P2PPeerListener.IP2PActionListener {
    String notificationTitle = "Successfully Message Delivered";
    String notificationBody = "Message delivered to: ";

    HomeActivity homeActivity;
    UserData db;

    double latitude = 0;
    double longitude = 0;
    int countTimer = 0;
    String name = "";
    String message = "";
    String contact1 = "";
    String whatsappMessage = "";

    String networkSSID = null;

    AirplaneModeBroadcastReceiver airplaneModeBroadcastReceiver;

    BroadcastReceiver deliveredMessageBroadcastReceiver = new DeliveredMessageBroadcastReceiver();
    String finalLocation = null;

    WifiP2pManager wifiP2pManager;
    WifiP2pManager.Channel channel;

    int notificationChannel = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

    public boolean isWifiP2pEnabled = false;

    public boolean startedDiscovery = false;

    P2POnConnection p2POnConnection;

    Context mContext;

    @Override
    public void onReceive(final Context context, Intent intent) {
        homeActivity = new HomeActivity();

        db = new UserData(context);

        this.mContext = context;

        // intent filters for wifi p2p broadcast receiver
        IntentFilter intentFilter2 = new IntentFilter();

        //wifi p2p status
        intentFilter2.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // checks for the change of the available peers.
        intentFilter2.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // checks the state of the connectivity
        intentFilter2.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // checks for device's status
        intentFilter2.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        //register p2p broadcast receiver
        BroadcastReceiver p2pBroadcastReceiver = new P2PBroadcastReceiver(wifiP2pManager, channel, SentMessageBroadcastReceiver.this);
        context.getApplicationContext().registerReceiver(p2pBroadcastReceiver, intentFilter2);

        wifiP2pManager = (WifiP2pManager) context.getApplicationContext().getSystemService(Context.WIFI_P2P_SERVICE);
        channel = wifiP2pManager.initialize(context, context.getApplicationContext().getMainLooper(), null);

        //get the data from db
        contact1 = db.gatherCOL_3().toString();
        message = db.gatherCOL_2().toString();
        name = db.gatherCOL_1().toString();
        whatsappMessage = "I'm " + name + " and " + message + " My location is: "
                //if comma doesnt work then use encoding for the link
                + "http://maps.google.com/?q=" + latitude + "," + longitude + "";

        String sent = "SENT";
        String delivered = "DELIVERED";
        //initialise pending intents for broadcast receiver to listen for whenever the message is sent and delivered
        final PendingIntent sentPendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(
                sent), 0);
        final PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(delivered), 0);

        context.registerReceiver(deliveredMessageBroadcastReceiver, new IntentFilter(delivered));

        //adapted from https://stackoverflow.com/questions/29118625/android-sms-manager-delivery-report-and-sent-report-not-received
        switch (getResultCode()) {
            case Activity.RESULT_OK:
                db.onClose();
                break;
            //if somewhat, in any of these errors occur then re-send the message within an interval
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                //failed somehow, cannot tell how
                try {
                    gatherLocation(context);
                    if (finalLocation == null) {

                    } else {
                        updateLocation(context);
                    }
                    db.OnOPen();
                    //timer to try re-sending the message after 5 seconds
                    final Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            //will stop after it sent
                            if (countTimer == 1) {
                                timer.cancel();
                                timer.purge();
                                db.onClose();
                            } else {
                                //send the message
                                homeActivity.sendMessage(name, message, contact1, sentPendingIntent, deliveredPendingIntent, latitude, longitude);
                                countTimer++;
                                db.onClose();
                            }
                        }
                        //sends after 5 seconds.
                    }, 5000);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
                break;
            //service is not available at the moment
            case SmsManager.RESULT_ERROR_NO_SERVICE:

                //adapted from https://stackoverflow.com/questions/29574730/how-to-connect-to-wifi-programmatically
                final WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                //turn on the wifi service
                wifiManager.setWifiEnabled(true);
                //declare wifi configuration to pick up available wifi around
                WifiConfiguration wifiConfiguration = new WifiConfiguration();
                wifiConfiguration.SSID = "\"" + networkSSID + "\"";
                //only open networks
                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                //add the network
                int networkID = wifiManager.addNetwork(wifiConfiguration);

                //if there is an available open network then connect
                if (networkID != -1) {
                    wifiManager.disconnect();
                    //enable and connect to network
                    wifiManager.enableNetwork(networkID, true);
                    wifiManager.reconnect();

                    Toast.makeText(context.getApplicationContext(), "Connected!", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(context.getApplicationContext(), "Unable to connect!", Toast.LENGTH_LONG).show();

                }

                try {
                    gatherLocation(context);
                    if (finalLocation == null) {

                    } else {
                        updateLocation(context);
                    }

                    //adapted from https://stackoverflow.com/questions/15462874/sending-message-through-whatsapp

                    //whatsapp tos -- "Be aware that the following actions are in violation of our Terms of Service:
                    //Using an automated system or an unauthorized / unofficial client application to send messages through WhatsApp."

                    //so it can only open the chat
                    //the user needs to press send thats all, not %100 automated
                    PackageManager packageManager = context.getPackageManager();
                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    //the url allows to send message to the phone number
                    String message = "https://api.whatsapp.com/send?phone=" + contact1 + "&text=" + URLEncoder.encode(whatsappMessage, "UTF-8");
                    //get to whatsapp and send the data when its open
                    intent1.setPackage("com.whatsapp");
                    intent1.setData(Uri.parse(message));
                    //if there is whatsapp, open it
                    if (intent1.resolveActivity(packageManager) != null) {
                        context.startActivity(intent1);
                    } else {
                        //adapted from https://developer.android.com/guide/topics/connectivity/wifip2p.html
                        //if not start wi-fi direct discovery
                        wifiP2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                            //
                            @Override
                            public void onSuccess() {
                                homeActivity.buildNotification(context, notificationChannel, R.drawable.ic_tick, Color.RED, "Success", "Discovery Initiated");
                                startedDiscovery = true;

                            }

                            @Override
                            public void onFailure(int reasonCode) {
                                homeActivity.buildNotification(context, notificationChannel, R.drawable.ic_tick, Color.RED, "Error", "Discovery Failed - " + reasonCode);

                            }
                        });

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            //error in pdu, something wrong in transmission process
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Toast.makeText(context, "PDU Error!", Toast.LENGTH_SHORT)
                        .show();
                break;
            //the phone is in airplane mode
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                try {
                    // Open the airplane mode settings to turn it off
                    Intent i = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.getApplicationContext().startActivity(i);

                    //if airplane mode is turned off then send message
                    airplaneModeBroadcastReceiver = new AirplaneModeBroadcastReceiver() {
                        @Override
                        public void airplaneModeChanged(boolean enabled) {
                            gatherLocation(context);
                            if (finalLocation == null) {

                            } else {
                                updateLocation(context);
                            }
                            //handler to delay process
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //send the message and notify the user
                                    db.OnOPen();

                                    homeActivity.sendMessage(name, message, contact1, sentPendingIntent, deliveredPendingIntent, latitude, longitude);
                                    homeActivity.buildNotification(context, 3, R.drawable.ic_tick, Color.RED, notificationTitle, notificationBody + contact1);
                                    db.onClose();
                                }
                                //after 5 seconds
                            }, 5000);
                        }
                    };
                    //register broadcast receiver to listen for user action
                    airplaneModeBroadcastReceiver.register(context);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }

    }

    //get the current location
    public void gatherLocation(Context context) {

        boolean is_gps_provider_enabled = false;
        boolean is_network_provider_enabled = false;

        Location gpsLocation = null;
        Location networkLocation = null;

        //location manager helps to get current location
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //setting boolean values for either gps or network availability
        is_gps_provider_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        is_network_provider_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {

            //https://developer.android.com/guide/topics/location/strategies.html
            try {
                if (is_gps_provider_enabled) {
                    //using network provider service
                    gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (is_network_provider_enabled) {
                    //using network provider service
                    networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                //if gps and network providers are in use then get the best accuracy and use that to provide location
                if (gpsLocation != null && networkLocation != null) {
                    //checking if network has a better accuracy first, if true then use the network provided service
                    if (gpsLocation.getAccuracy() > networkLocation.getAccuracy()) {
                        //get the values to send via message
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    } else {   //otherwise use gps provider
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    }
                }
                //if one of the provider is disabled then use the other
                else {
                    if (gpsLocation != null) {
                        latitude = gpsLocation.getLatitude();
                        longitude = gpsLocation.getLongitude();
                    } else if (networkLocation != null) {
                        latitude = networkLocation.getLatitude();
                        longitude = networkLocation.getLongitude();
                    }
                }

            } catch (Exception ec) {
                ec.printStackTrace();
            }

        } else

        {


        }

    }

    public void updateLocation(Context context) {

        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        final int loc_update = Integer.parseInt(db.gatherCOL_7().toString()) * 1000;
        //location permission
        int permissionLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            try

            {
                final LocationListener locationListener = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();

                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {
                        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        }

                    }

                    @Override
                    public void onProviderDisabled(String s) {
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            finalLocation = LocationManager.NETWORK_PROVIDER;
                        } else if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            finalLocation = LocationManager.GPS_PROVIDER;
                        }
                    }

                };
                //updates the location and the level of zoom level. 20 is buildings level
                locationManager.requestLocationUpdates(finalLocation, loc_update, 20, locationListener);
            } catch (
                    Exception exce)

            {
                exce.printStackTrace();
            }
        } else

        {
            //ask for permission if access is not given
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);

        }

    }

    //adapted from https://developer.android.com/guide/topics/connectivity/wifip2p.html
    @Override
    public void P2PConnect() {

        //try connecting with the peer without a password
        wifiP2pManager.createGroup(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
//                System.out.println("Connection established");
//                homeActivity.buildNotification(mContext, notificationChannel, R.drawable.ic_tick, Color.RED, "Success", "Connection Established");

            }

            @Override
            public void onFailure(int reason) {
//                System.out.println("Connection failed " + reason);
//                homeActivity.buildNotification(mContext, notificationChannel, R.drawable.ic_tick, Color.RED, "Error", "Connection failed: " + reason);

            }
        });

    }

}
