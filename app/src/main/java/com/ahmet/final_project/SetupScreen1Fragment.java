package com.ahmet.final_project;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SetupScreen1Fragment extends Fragment {

    Button nextButton;
    Button backButton;
    EditText nameText;
    EditText messageText;

    private static final int PERMISSION_REQUEST_SEND_SMS = 0;
    UserData db;


    public SetupScreen1Fragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.setup_1, container, false);

        db = new UserData(getActivity());

        //initialise the buttons and edittext
        nextButton = (Button) view.findViewById(R.id.setup1NextButton);
        backButton = (Button) view.findViewById(R.id.setup1BackButton);
        nameText = (EditText) view.findViewById(R.id.nameEdittext);
        messageText = (EditText) view.findViewById(R.id.setMessageEditText);

        messageText.setEnabled(false);
        //get the permission from manifest to compare
        int permissionSMS = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS);

        if (permissionSMS == PackageManager.PERMISSION_GRANTED) {
        } else {
            //ask for permission if access is not given
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST_SEND_SMS);

        }

        FirstTimeUseActivity firstTimeUseActivity = (FirstTimeUseActivity) getActivity();

        //get the static data to show in fields if the user decides to go back
        String name = firstTimeUseActivity.getName().toString();
        String message = firstTimeUseActivity.getMessage().toString();
        //set the data in fields
        nameText.setText(name);
        messageText.setText(message);

        //when clicked next after filled, go next page
        nameText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                if (action == EditorInfo.IME_ACTION_NEXT) {
                    String checkNameText = nameText.getText().toString();
                    String message = messageText.getText().toString();

                    //check if textboxes are empty
                    if (checkNameText.equals("") || message.equals("")) {
                        //if empty then output toast
                        Toast.makeText(getActivity(), "Fields need to be filled in order to continue!", Toast.LENGTH_LONG).show();
                    } else {

                        completeSetup();
                        //hides the keyboard
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    }
                    return true;
                }

                return false;
            }
        });

        goNextPage();
        goBack();
        return view;
    }

    public void goNextPage() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                completeSetup();

            }
        });
    }

    public void completeSetup() {
        String checkNameText = nameText.getText().toString();
        String message = messageText.getText().toString();

        //check if textboxes are empty
        if (checkNameText.equals("") || message.equals("")) {
            //if empty then output toast
            Toast.makeText(getActivity(), "Fields need to be filled in order to continue!", Toast.LENGTH_LONG).show();
        } else {

            FirstTimeUseActivity firstTimeUseActivity = (FirstTimeUseActivity) getActivity();
            //adding the input fields to the related setter in firstTimeUseActivity activity,
            //so at the very end of the setup it can be pulled from the getter to add into the db
            String setName = checkNameText;
            firstTimeUseActivity.setName(setName);

            String setMessage = message;
            firstTimeUseActivity.setMessage(setMessage);

            //opens the next setup
            SetupScreen2Fragment setup_Screen_2Fragment = new SetupScreen2Fragment();
            FragmentManager manager = getFragmentManager();
            //replacing the fragment inside the layout
            manager.beginTransaction().replace(R.id.layout_Fragment, setup_Screen_2Fragment).addToBackStack(null).commit();
        }
    }

    public void goBack() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //opens the previous page
                Permission_Fragment permission_fragment = new Permission_Fragment();
                FragmentManager manager = getFragmentManager();
                //replacing the fragment inside the layout
                manager.beginTransaction().replace(R.id.layout_Fragment, permission_fragment).addToBackStack(null).commit();

            }
        });
    }

    //getting the grant from the user and handling the response
    @Override
    public void onRequestPermissionsResult(int request, String[] permission, int[] grant) {
        super.onRequestPermissionsResult(request, permission, grant);
        switch (request) {
            case PERMISSION_REQUEST_SEND_SMS: {
                if (grant.length == 1 && grant[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getActivity(), "Application will not fully function if you don't grant permissions", Toast.LENGTH_LONG).show();
                }

            }
        }
    }


}
