package com.ahmet.final_project;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class SetupScreen2Fragment extends Fragment {

    Button nextButton;
    Button backButton;
    Button addContactB1;

    Spinner loc_update;
    Spinner loc_refresh;
    Spinner loc_history;

    TextView contactText1;
    TextView contactText2;
    TextView contactText3;


    private String contactID;
    private static final int CONTACT_REQUEST_CODE = 1;

    private static final int PERMISSION_REQUEST_CONTACTS = 0;

    FirstTimeUseActivity firstTimeUseActivity;

    public SetupScreen2Fragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setup_2, container, false);

        nextButton = (Button) view.findViewById(R.id.setup2NextButton);
        backButton = (Button) view.findViewById(R.id.setup2BackButton);
        addContactB1 = (Button) view.findViewById(R.id.addContactButton1);

        loc_update = (Spinner) view.findViewById(R.id.updateSpinner);
        loc_refresh = (Spinner) view.findViewById(R.id.refreshSpinner);
        loc_history = (Spinner) view.findViewById(R.id.historySpinner);

        contactText1 = (TextView) view.findViewById(R.id.contactTextview1);
        contactText2 = (TextView) view.findViewById(R.id.contactTextview2);
        contactText3 = (TextView) view.findViewById(R.id.contactTextview3);

        //adapted from https://developer.android.com/guide/topics/ui/controls/spinner.html
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.location_update, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loc_update.setAdapter(adapter);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(),
                R.array.refresh_location, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loc_refresh.setAdapter(adapter2);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(getActivity(),
                R.array.location_history, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loc_history.setAdapter(adapter3);

        int permissionContacts = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);

        if (permissionContacts == PackageManager.PERMISSION_GRANTED) {
        } else {
            //ask for permission if access is not given
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CONTACTS);

        }

        ImageButton locationUpdateInformationButton = (ImageButton) view.findViewById(R.id.locationUpdateInformationButton);

        //information dialog box within
        locationUpdateInformationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.information_dialog_box, null);

                EditText informationText = (EditText) view1.findViewById(R.id.informationEditText);
                //providing information
                String information = getResources().getString(R.string.location_update_Information);
                informationText.setText(information);
                //set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(false);
                //create clickable button
                dialog.setPositiveButton("Okay", null);

                final AlertDialog alert = dialog.create();
                alert.show();
            }
        });


        firstTimeUseActivity = (FirstTimeUseActivity) getActivity();
        setHasOptionsMenu(true);

        //get the static data to show in fields if the user decides to go back
        String contact1 = firstTimeUseActivity.getContact1().toString();
        String contact2 = firstTimeUseActivity.getContact2().toString();
        String contact3 = firstTimeUseActivity.getContact3().toString();

        //set the numbers
        contactText1.setText(contact1);
        contactText2.setText(contact2);
        contactText3.setText(contact3);

        addContacts();
        goNextPage();
        goBack();
        return view;
    }

    public void goNextPage() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String sContactcheck = contactText1.getText().toString();
                if (sContactcheck.equals("")) {
                    //if empty then output toast
                    Toast.makeText(getActivity(), "At least 1 contact needs to be selected", Toast.LENGTH_LONG).show();
                } else {
                    FirstTimeUseActivity firstTimeUseActivity = (FirstTimeUseActivity) getActivity();
                    //adding the input fields to the related setter in firstTimeUseActivity activity,
                    String setLoca_update = loc_update.getSelectedItem().toString();
                    firstTimeUseActivity.setLoc_update(setLoca_update);

                    String setLoca_refresh = loc_refresh.getSelectedItem().toString();
                    firstTimeUseActivity.setLoc_refresh(setLoca_refresh);

                    String setLoca_history = loc_history.getSelectedItem().toString();
                    firstTimeUseActivity.setLoc_history(setLoca_history);

                    //opens the next setup
                    SetupScreen3Fragment setup_Screen_3Fragment = new SetupScreen3Fragment();
                    FragmentManager manager = getFragmentManager();
                    //replacing the fragment inside the layout
                    manager.beginTransaction().replace(R.id.layout_Fragment, setup_Screen_3Fragment).commit();
                }
            }
        });
    }

    public void goBack() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //opens the previous page
                SetupScreen1Fragment setup_Screen_1Fragment = new SetupScreen1Fragment();
                FragmentManager manager = getFragmentManager();
                //replacing the fragment inside the layout
                manager.beginTransaction().replace(R.id.layout_Fragment, setup_Screen_1Fragment).addToBackStack(null).commit();
            }
        });
    }

    //inflating the menu on action bar within fragment
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.manual_contact, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    //action bar button options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //when clicked, it opens settings dialog box
            case R.id.manualAddContactActionBar:

                //new alert dialog
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                //inflate the dialog layout to import it into alert dialog in order to view
                LayoutInflater layoutInflater = getLayoutInflater();
                //when inflated, then able to view the settings screen
                View view1 = layoutInflater.inflate(R.layout.add_contact_manually, null);
                //initialise buttons and textviews
                final EditText contact1EditText = (EditText) view1.findViewById(R.id.contact1EditText);
                final EditText contact2EditText = (EditText) view1.findViewById(R.id.contact2EditText);
                final EditText contact3EditText = (EditText) view1.findViewById(R.id.contact3EditText);

                //set custom layout in dialog
                dialog.setView(view1);

                dialog.setCancelable(true);
                dialog.setPositiveButton("Add Contacts", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String checkContact1 = contact1EditText.getText().toString();
                        String checkContact2 = contact2EditText.getText().toString();
                        String checkContact3 = contact3EditText.getText().toString();

                        //if the edittext is empty then warn the user and dont add
                        if (checkContact1.equals("")){
                            Toast.makeText(getActivity(), "At least 1 contact needs to be entered to continue.", Toast.LENGTH_LONG).show();
                        }
                        //if contact 1 doesnt start with 0 then warn the user and dont add
                        else if (!checkContact1.startsWith("0")){
                            Toast.makeText(getActivity(), "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        //if contact 2 isnt empty and doesnt start with 0 then dont add
                        else if (!checkContact2.equals("") && !checkContact2.startsWith("0")){
                            Toast.makeText(getActivity(), "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        //if contact 3 isnt empty and doesnt start with 0 then dont add
                        else if (!checkContact3.equals("") && !checkContact3.startsWith("0")){
                            Toast.makeText(getActivity(), "You need to start with '0'", Toast.LENGTH_LONG).show();

                        }
                        else
                        {
                            //add it to contact text view
                            contactText1.setText(checkContact1);
                            //add it to the setter
                            firstTimeUseActivity.setContact1(checkContact1);

                            //add it to contact text view
                            contactText2.setText(checkContact2);
                            //add it to the setter
                            firstTimeUseActivity.setContact2(checkContact2);

                            //add it to contact text view
                            contactText3.setText(checkContact3);
                            //add it to the setter
                            firstTimeUseActivity.setContact3(checkContact3);

                            Toast.makeText(getActivity(), "Contacts added!", Toast.LENGTH_LONG).show();
                        }

                    }
                });
                dialog.setNegativeButton("Cancel", null);

                //create and show the dialog
                final AlertDialog alert = dialog.create();
                alert.show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    //getting the grant from the user and handling the response
    @Override
    public void onRequestPermissionsResult(int request, String[] permission, int[] grant) {
        super.onRequestPermissionsResult(request, permission, grant);
        switch (request) {

            case PERMISSION_REQUEST_CONTACTS: {
                if (grant.length == 1 && grant[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getActivity(), "Application will not fully function if you don't grant permissions", Toast.LENGTH_SHORT).show();
                }

            }

        }
    }

    //adapted from https://stackoverflow.com/questions/15620805/how-to-select-multiple-contacts-from-the-phone-using-checkboxes

    private void addContacts() {

        addContactB1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //opens the contacts list
                //opens contact list and allows the user to select limited multiple contacts
                Intent contactList = new Intent("intent.action.INTERACTION_TOPMENU");
                contactList.putExtra("additional", "phone-multi");
                contactList.putExtra("maxRecipientCount", 4);
                contactList.putExtra("FromMMS", true);

                startActivityForResult(contactList, CONTACT_REQUEST_CODE);
            }
        });

    }
    //https://stackoverflow.com/questions/15620805/how-to-select-multiple-contacts-from-the-phone-using-checkboxes
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CONTACT_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {

            Bundle bundle = data.getExtras();

            String result = bundle.getString("result");
            ArrayList<String> contacts = bundle.getStringArrayList("result");

            //if max amount of contacts arent selected then only add the ones that are selected
            if (contacts.size() == 1) {
                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();
                //add it to contact text view
                contactText1.setText(sContact);
                //add it to the setter
                firstTimeUseActivity.setContact1(sContact);
            } else {

            }
            if (contacts.size() == 2) {
                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();
                //add it to contact text view
                contactText1.setText(sContact);
                //add it to the setter
                firstTimeUseActivity.setContact1(sContact);

                //only add the number and trim the rest which shows the id.
                String contact2 = contacts.get(1).toString();
                String sContact2 = contact2.substring(contact2.indexOf(";") + 1);
                sContact2.trim();
                //add it to contact text view
                contactText2.setText(sContact2);
                //add it to the setter
                firstTimeUseActivity.setContact2(sContact2);
            } else {

            }
            if (contacts.size() == 3) {
                //only add the number and trim the rest which shows the id.
                String contact = contacts.get(0).toString();
                String sContact = contact.substring(contact.indexOf(";") + 1);
                sContact.trim();
                //add it to contact text view
                contactText1.setText(sContact);
                //add it to the setter
                firstTimeUseActivity.setContact1(sContact);

                //only add the number and trim the rest which shows the id.
                String contact2 = contacts.get(1).toString();
                String sContact2 = contact2.substring(contact2.indexOf(";") + 1);
                sContact2.trim();
                //add it to contact text view
                contactText2.setText(sContact2);
                //add it to the setter
                firstTimeUseActivity.setContact2(sContact2);

                //only add the number and trim the rest which shows the id.
                String contact3 = contacts.get(2).toString();
                String sContact3 = contact3.substring(contact3.indexOf(";") + 1);
                sContact3.trim();
                //add it to contact text view
                contactText3.setText(sContact3);
                //add it to the setter
                firstTimeUseActivity.setContact3(sContact3);
            } else {

            }

        }

    }

    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
