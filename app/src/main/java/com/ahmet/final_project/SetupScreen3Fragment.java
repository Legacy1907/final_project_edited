package com.ahmet.final_project;


import android.Manifest;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class SetupScreen3Fragment extends Fragment {

    Button finish;
    Button backButton;
    Spinner power_button_press_spinner;

    private static final int PERMISSION_REQUEST_LOCATION = 0;

    public static final int PERMISSIONS = 3;

    String[] multiple_permissions = new String[]{
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_FINE_LOCATION};

    //initialise list to put permissions into a list
    List<String> permissionsList = new ArrayList<>();

    UserData db;

    public SetupScreen3Fragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setup_3, container, false);

        db = new UserData(getActivity());

        finish = (Button) view.findViewById(R.id.finishButtonSetup3);
        backButton = (Button) view.findViewById(R.id.setup3BackButton);

        power_button_press_spinner = (Spinner) view.findViewById(R.id.power_spinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.power_button_press, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        power_button_press_spinner.setAdapter(adapter);

        finishWholeSetup();
        goBack();
        return view;
    }

    public void finishWholeSetup() {
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //finish setup if permissions are granted
                if (checkPermissions()) {

                    //adapted from -- https://stackoverflow.com/questions/31638986/protected-apps-setting-on-huawei-phones-and-how-to-handle-it
                    //for huawei devices, the application needs to be one of the 'protected app' in order for the service to work.
                    //the user needs to do this in order for the notification to appear when the activity is killed

                    //initialise shared preferences
                    final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("ProtectedApps", getActivity().MODE_PRIVATE);
                    //check if its a huawei device
                    if ("huawei".equalsIgnoreCase(android.os.Build.MANUFACTURER) && !sharedPreferences.getBoolean("ProtectedApps", false)) {
                        //create alert dialog notifying the user what it needs to be done
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("One last step..").setMessage("Please allow this app to be a 'Protected App' because you won't be able to use the SOS feature from notification.")
                                .setPositiveButton("Head to Settings", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //open the 'Protected app' settings
                                        Intent intent = new Intent();
                                        intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
                                        startActivity(intent);
                                        //make it true if opened
                                        sharedPreferences.edit().putBoolean("ProtectedApps", true).commit();

                                    }
                                }).create().show();
                        //if the app is placed under protected apps then continue
                    } else if ("huawei".equalsIgnoreCase(android.os.Build.MANUFACTURER) && sharedPreferences.getBoolean("ProtectedApps", false)) {
                        completeCurrentSetup();
                    }
                    //for other android devices, juts complete the setup
                    else {
                        completeCurrentSetup();
                    }
                } else

                {
                    showMessage("Permissions", "Please allow the requested permissions in order for the application to serve you.");
                }

            }

        });
    }

    private void completeCurrentSetup() {
        //initialise dialog box
        DialogInterface.OnClickListener dialogInterface = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    //complete the setup if confirmed
                    case DialogInterface.BUTTON_POSITIVE:

                        try {
                            FirstTimeUseActivity firstTimeUseActivity = (FirstTimeUseActivity) getActivity();
                            //adding the input fields to the related setter in firstTimeUseActivity activity,
                            String setPower = power_button_press_spinner.getSelectedItem().toString();
                            firstTimeUseActivity.setPower_press(setPower);

                            //getting the static data from getters in firstTimeUseActivity activity set from the previous setup pages and adding it to the database.
                            String sName = firstTimeUseActivity.getName();
                            String sMessage = firstTimeUseActivity.getMessage();
                            String sContact = firstTimeUseActivity.getContact1();
                            String sContact2 = firstTimeUseActivity.getContact2();
                            String sContact3 = firstTimeUseActivity.getContact3();
                            String sContact4 = firstTimeUseActivity.getContact4();
                            String sLoc_update = firstTimeUseActivity.getLoc_update();
                            String sLoc_refresh = firstTimeUseActivity.getLoc_refresh();
                            String sLoc_history = firstTimeUseActivity.getLoc_history();
                            String sPower = firstTimeUseActivity.getPower_press();
                            String isnotfirsttime = "1";
                            boolean insertedData = db.addFromSetup(sName, sMessage, sContact, sContact2, sContact3, sContact4, sLoc_update, sLoc_refresh, sLoc_history, sPower, isnotfirsttime);
                            if (insertedData == true) {

                                //complete setup and open homeActivity page
                                startActivity(new Intent(getActivity(), HomeActivity.class));
                                getActivity().finish();
                                Toast.makeText(getContext(), "Setup completed!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception exc) {
                            exc.printStackTrace();
                            showMessage("Error", exc.getMessage());
                        }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Do you want to complete the setup?").setPositiveButton("Yes", dialogInterface)
                .setNegativeButton("No", dialogInterface).show();

    }

    public void goBack() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //opens the previous page
                SetupScreen2Fragment setup_Screen_2Fragment = new SetupScreen2Fragment();
                FragmentManager manager = getFragmentManager();
                //replacing the fragment inside the layout
                manager.beginTransaction().replace(R.id.layout_Fragment, setup_Screen_2Fragment).addToBackStack(null).commit();
            }
        });
    }

    //adapted from -- https://stackoverflow.com/questions/34342816/android-6-0-multiple-permissions
    //check for permissions if granted
    private boolean checkPermissions() {
        int result;

        for (String permission : multiple_permissions) {
            //check if permissions are granted
            result = ContextCompat.checkSelfPermission(getActivity(), permission);
            //add to the list of not granted permissions
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
            }
        }
        //if the list isn't empty then ask for those permissions
        if (!permissionsList.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), permissionsList.toArray(new String[permissionsList.size()]), PERMISSIONS);
            //clear the list because the list will be filled with permissions again if not granted but this causes to not complete the setup.
            //So clearing the list will update it.
            permissionsList.clear();
            return false;
        }
        if (permissionsList.isEmpty()) {
            return true;
        }

        return true;
    }


    //getting the grant from the user and handling the response
    @Override
    public void onRequestPermissionsResult(int request, String[] permission, int[] grant) {
        super.onRequestPermissionsResult(request, permission, grant);
        switch (request) {

            case PERMISSIONS: {
                //permissions granted
                if (grant.length > 0 && grant[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    //show the permissions that aren't yet granted
                    String permissions = "";
                    for (String per : permissionsList) {
                        permissions += "\n" + per;
                    }
                }
                return;
            }

        }
    }

    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

}
